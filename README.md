# RP Tools

A collection of small silly tools for my roleplaying games (Semi-Real and Tabletop RPG)

## Rock Paper Scissors

Since I do semi-real Vampire: The Masquerade, I made a small rock paper scissors generator for when I'm asked for a bunch of them by email for actions between sessions. You just need to run the following command in your terminal (Linux, Mac, WSL2 on Windows 10/11).

Example here for 13 rock paper scissors games:

```shell
./scripts/shifumi.sh 13
```

And if I need to put the results in a file, I personally add a little `>shifumi.txt`

## Rock Paper Scissors Lizard Spock

For Big Bang Theory fans, I've integrated Sheldon's variant in a script called rpsls.sh (everything else works the same)

## Web version

Go to Pipelines, launch New Pipeline, add "shifumis" as variable key, and the number of shifumis you need in variable value