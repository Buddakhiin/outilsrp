#!/bin/bash
# add the number of shifumi you need
nbShifumi=$1
# the possible results
ResultShifumi=("Pierre" "Feuille" "Ciseaux" "Lézard" "Spock")
# seed random generator
RANDOM=$$$(date +%s)
#initialize incremential variable
i=1

while [ $i -le $nbShifumi ]
do
    echo ${ResultShifumi[$RANDOM % ${#ResultShifumi[@]}]}
    ((i++))
done
